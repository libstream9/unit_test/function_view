#define BOOST_TEST_MODULE main
#include <boost/test/unit_test.hpp>

#include <stream9/function_view.hpp>

namespace testing {

using stream9::function_view;

BOOST_AUTO_TEST_SUITE(function_ptr_)

    static int f1() { return 5; }

    BOOST_AUTO_TEST_CASE(basic_)
    {
        function_view<int()> fv { f1 };

        BOOST_TEST(fv() == 5);
    }

    BOOST_AUTO_TEST_CASE(copy_construction_)
    {
        function_view<int()> fv1 { f1 };

        function_view<int()> fv2 { fv1 };
        BOOST_TEST(fv2() == 5);
    }

    BOOST_AUTO_TEST_CASE(move_construction_)
    {
        function_view<int()> fv1 { f1 };

        function_view<int()> fv2 { std::move(fv1) };
        BOOST_TEST(fv2() == 5);
    }

    BOOST_AUTO_TEST_CASE(copy_assignment_)
    {
        function_view<int()> fv1 { f1 };

        function_view<int()> fv2;
        fv2 = fv1;

        BOOST_TEST(fv2() == 5);
    }

    BOOST_AUTO_TEST_CASE(move_assignment_)
    {
        function_view<int()> fv1 { f1 };

        function_view<int()> fv2;
        fv2 = std::move(fv1);

        BOOST_TEST(fv2() == 5);
    }

    BOOST_AUTO_TEST_CASE(boolean_test_)
    {
        function_view<int()> fv;

        BOOST_TEST(!fv);

        fv = f1;
        BOOST_TEST(!!fv);
    }

    BOOST_AUTO_TEST_CASE(deduction_guide_)
    {
        function_view fv = f1;

        static_assert(std::same_as<decltype(fv), function_view<int()>>);
    }

    static short f2() { return 2; }

    BOOST_AUTO_TEST_CASE(convertible_return_value_)
    {
        function_view<int()> fv { f2 };

        BOOST_TEST(fv() == 2);
    }

BOOST_AUTO_TEST_SUITE_END() // function_ptr_

BOOST_AUTO_TEST_SUITE(function_object_)

    BOOST_AUTO_TEST_CASE(lambda_)
    {
        auto f = [](){ return 1; };
        function_view<int()> fv { f };

        BOOST_TEST(fv() == 1);
    }

    static int f1(function_view<int(int)> const fv)
    {
        return fv(5);
    }

    BOOST_AUTO_TEST_CASE(rvalue_)
    {
        auto const rv = f1([](auto i) { return i * 2; });

        BOOST_TEST(rv == 10);
    }

    BOOST_AUTO_TEST_CASE(deduction_guide_)
    {
        auto f = []() { return 1; };
        function_view fv = f;

        static_assert(std::same_as<decltype(fv), function_view<int()>>);
    }

BOOST_AUTO_TEST_SUITE_END() // function_object_

} // namespace testing
